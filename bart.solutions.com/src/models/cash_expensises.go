package models

type CashExpenses struct {
	Product       string
	Home          string
	CityTransport string
	OwnTransport  string
	FlatRent      string
	Lend          string
}
