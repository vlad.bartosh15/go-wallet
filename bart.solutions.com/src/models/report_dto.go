package models

type ReportDto struct {
	CashExpenses CashExpenses
	MoneyIncome  MoneyIncome
}
